﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
	void Start()
	{
		if (SceneManager.GetActiveScene().name == "Loading")
		{
			string _scene = PlayerPrefs.GetString("scene");
			if (_scene != "")
			{
				PlayerPrefs.SetString("scene", "");
				SceneManager.LoadSceneAsync(_scene);
			}
		}
	}

	public void SceneToLoad(string _scene)
	{
		PlayerPrefs.SetString("scene", _scene);
	}

	public void SceneToLoadAndLoad(string _scene)
	{
		PlayerPrefs.SetString("scene", _scene);
		SceneManager.LoadSceneAsync("Loading");
	}

	public void SceneLoadNow()
	{
		SceneManager.LoadSceneAsync("Loading");
	}

	public void LoadSceneRaw(string _scene)
	{
		SceneManager.LoadSceneAsync(_scene);
	}
}